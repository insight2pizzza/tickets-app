package app.controller;

import lombok.RequiredArgsConstructor;
import app.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import app.service.UserService;

import java.util.List;

@RestController
@RequestMapping(value = "/api")
@RequiredArgsConstructor
public class UserController {
    @Autowired
    UserService userService;

    @GetMapping("/user")
    private List<User> getAllUsers() {
        return userService.getAllUsers();
    }

    @GetMapping("/user/{id}")
    private User getUser(@PathVariable("id") int id) {
        return userService.getUserById(id);
    }

    @DeleteMapping("/user/{id}")
    private void deleteUser(@PathVariable("id") int id) {
        userService.delete(id);
    }

    @PostMapping("/user")
    private int saveUser(@RequestBody User user) {
        userService.save(user);
        return user.getId();
    }

    @PutMapping("/user")
    private User updateUser (@RequestBody User user, int id) {
        userService.update(user, id);
        return user;
    }
}
